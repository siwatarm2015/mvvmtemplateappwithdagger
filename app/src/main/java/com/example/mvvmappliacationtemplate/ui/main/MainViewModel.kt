package com.example.mvvmappliacationtemplate.ui.main

import androidx.lifecycle.ViewModel
import com.example.mvvmappliacationtemplate.repository.UserRepository
import com.example.mvvmappliacationtemplate.testing.OpenForTesting
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import javax.inject.Inject

@OpenForTesting
class MainViewModel @Inject constructor(userRepository: UserRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val result = userRepository.loadUser().subscribe({
            Timber.d("ssss")
        },{
            it.printStackTrace()
        }).addTo(compositeDisposable)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}

