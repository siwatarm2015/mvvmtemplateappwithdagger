package com.example.mvvmappliacationtemplate.ui.common
/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
