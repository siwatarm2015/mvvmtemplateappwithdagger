package com.example.mvvmappliacationtemplate.model

data class User(val name: String? = null,
                val surname: String? = null,
                val age: Int? = 0)