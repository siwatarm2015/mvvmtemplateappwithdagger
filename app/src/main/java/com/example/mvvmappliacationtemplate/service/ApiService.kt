package com.example.mvvmappliacationtemplate.service

import com.example.mvvmappliacationtemplate.model.User
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * REST API access points
 */

interface ApiService {

    @GET("")
    fun getUser():Observable<ApiResponse<User>>

}