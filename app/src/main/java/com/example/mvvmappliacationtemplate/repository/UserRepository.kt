package com.example.mvvmappliacationtemplate.repository

import com.example.mvvmappliacationtemplate.model.User
import com.example.mvvmappliacationtemplate.service.ApiResponse
import com.example.mvvmappliacationtemplate.service.ApiService
import com.example.mvvmappliacationtemplate.testing.OpenForTesting
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles User objects.
 */
@OpenForTesting
@Singleton
class UserRepository @Inject constructor(
    private val apiService: ApiService
) {

    fun loadUser(): Observable<ApiResponse<User>> {
        return apiService.getUser()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
    }

}