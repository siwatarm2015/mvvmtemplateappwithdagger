package com.example.mvvmappliacationtemplate.di

import com.example.mvvmappliacationtemplate.ui.main.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeRepoFragment(): MainFragment
}
