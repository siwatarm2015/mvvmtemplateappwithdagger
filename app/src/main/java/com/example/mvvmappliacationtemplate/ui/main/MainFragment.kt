package com.example.mvvmappliacationtemplate.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.mvvmappliacationtemplate.R
import com.example.mvvmappliacationtemplate.binding.FragmentDataBindingComponent
import com.example.mvvmappliacationtemplate.databinding.MainFragmentBinding
import com.example.mvvmappliacationtemplate.di.Injectable
import com.example.mvvmappliacationtemplate.testing.OpenForTesting
import com.example.mvvmappliacationtemplate.ui.common.RetryCallback
import com.example.mvvmappliacationtemplate.util.autoCleared
import javax.inject.Inject

@OpenForTesting
class MainFragment : Fragment(),Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private var binding by autoCleared<MainFragmentBinding>()

    lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment,
            container,
            false,
            dataBindingComponent
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mainViewModel = ViewModelProviders.of(this,viewModelFactory)
            .get(MainViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewController = this

        binding.callback = object : RetryCallback {
            override fun retry() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }



    }

}
